/* Your JS here. */
//
window.onscroll = function() {navbarTrack()};

// Position Indicator
// let navbar = document.getElementById("topnav");

function navbarTrack() {
    let navLinks = document.querySelectorAll("nav a"); // Collects all a aelements in nav
    scrollPos = window.scrollY;
    navLinks.forEach(link => { // Syntax of for loops in JS
        let section = document.querySelector(link.hash);
        if (scrollPos + 10 > section.offsetTop && scrollPos + 10 < section.offsetTop + section.offsetHeight ) {
            link.classList.add("active");
        } else {
            link.classList.remove("active");
        }
    });
}

// NavBar Shrinking
document.addEventListener("scroll", e=> {
    let header = document.getElementById("home").clientHeight
    scrollPos = window.scrollY;
    if (scrollPos > document.body.offsetTop + header) {
        document.getElementById("topnav").style.padding = "5px 20px";
    } else {
        document.getElementById("topnav").style.padding = "20px 20px";
    }
});

// Carousel
const buttonleft = document.getElementById("button-left"); //Use data attribute instead of class to avoid class/JS overlap (?)
const buttonright = document.getElementById("button-right"); //Use data attribute instead of class to avoid class/JS overlap (?)
console.log("left", buttonleft);
console.log("right", buttonright);
let newIdx = 1;
let prevIdx = 0;
slideUpdate(newIdx, prevIdx);

function slideUpdate(next, prev) {
    console.log("NewIdx: ", next, "  PrevIdx: ", prev);
    // TODO: Make this more elegant/scalelable when you have time
    const slide1 = document.getElementById("slide1");
    const slide2 = document.getElementById("slide2");
    const slide3 = document.getElementById("slide3");
    const slides = [slide1, slide2, slide3];
    console.log("Slides length", slides.length)

    // Overflow Prevention
    if (next < 0) {
        newIdx = slides.length - 1;
        next = newIdx;
        console.log("Overflow Low, new idx: ", next);
    }
    if (next >= slides.length) {
        newIdx = 0;
        next = newIdx;
        console.log("Overflow High, new idx: ", next);
    }
    // Add/remove active slide
    slides[prevIdx].style.display = "none";
    slides[newIdx].style.display = "flex";
}

buttonleft.addEventListener("click", () => {
    console.log("Left");
    prevIdx = newIdx;
    newIdx -= 1;
    slideUpdate(newIdx, prevIdx);
});

buttonright.addEventListener("click", () => {
    console.log("Right");
    prevIdx = newIdx;
    newIdx += 1;
    slideUpdate(newIdx, prevIdx);
});

// Modal
const openModal1 = document.getElementById('modal1-open');
const closeModal1 = document.getElementById('modal1-close');
const modal1 = document.getElementById('modal1');

openModal1.addEventListener('click', () => {
    modal1.showModal();
})

closeModal1.addEventListener('click', () => {
    modal1.close();
})

const openModal2 = document.getElementById('modal2-open');
const closeModal2 = document.getElementById('modal2-close');
const modal2 = document.getElementById('modal2');

openModal2.addEventListener('click', () => {
    modal2.showModal();
})

closeModal2.addEventListener('click', () => {
    modal2.close();
})

const openModal3 = document.getElementById('modal3-open');
const closeModal3 = document.getElementById('modal3-close');
const modal3 = document.getElementById('modal3');

openModal3.addEventListener('click', () => {
    modal3.showModal();
})

closeModal3.addEventListener('click', () => {
    modal3.close();
})
